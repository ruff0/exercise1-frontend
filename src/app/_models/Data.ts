

export interface Data {
    id: string;
    name: string;
    value: number;
}
