import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DatasListComponent } from './datas/datas-list/datas-list.component';
import { DatasListResolver } from './_resolvers/datas-list.resolver';
import { DataDetailComponent } from './datas/data-detail/data-detail.component';
import { DataDetailResolver } from './_resolvers/data-detail.resolver';
import { DataEditComponent } from './datas/data-edit/data-edit.component';
import { DataEditResolver } from './_resolvers/data-edit.resolver';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { DataAddComponent } from './datas/data-add/data-add.component';


const routes: Routes = [
  { path: '', component: DatasListComponent, resolve: {datas: DatasListResolver} },
  { path: 'data', component: DatasListComponent, resolve: {datas: DatasListResolver} },
  { path: 'data/add', component: DataAddComponent },
  { path: 'data/:id', component: DataDetailComponent, resolve: { data: DataDetailResolver}},
  { path: 'data/edit/:id', component: DataEditComponent,
  resolve: { data: DataEditResolver}, canDeactivate: [PreventUnsavedChanges]},
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
