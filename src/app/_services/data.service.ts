import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Data } from '../_models/Data';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DataService {
  baseUrl = environment.apiUrl + 'data/';

  constructor(private http: HttpClient) {}

  getDatas(): Observable<Data[]> {
    return this.http.get<Data[]>(this.baseUrl, httpOptions);
  }

  getData(id): Observable<Data> {
    return this.http.get<Data>(this.baseUrl + id, httpOptions);
  }

  createData(data: Data) {
    console.log(data);
    return this.http.post(this.baseUrl, data, httpOptions);
  }

  updateData(id: string, data: Data) {
    return this.http.put(this.baseUrl + id +'/', data, httpOptions);
  }

  deleteData(id): Observable<number> {
    return this.http.delete<number>(this.baseUrl + id, httpOptions);
  }

}
