import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { DataEditComponent } from '../datas/data-edit/data-edit.component';

@Injectable()
export class PreventUnsavedChanges implements CanDeactivate<DataEditComponent> {

    canDeactivate(component: DataEditComponent) {
        if (component.editForm.dirty) {
            return confirm('Are you sure you want to continue? Any unsaved change will be lost');
        }
        return true;
    }
}
