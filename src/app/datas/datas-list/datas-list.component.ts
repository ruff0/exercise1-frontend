import { Component, OnInit } from '@angular/core';
import { DataService } from '../../_services/data.service';
import { Data } from '../../_models/Data';
import { AlertifyService } from '../../_services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-datas-list',
  templateUrl: './datas-list.component.html',
  styleUrls: ['./datas-list.component.css']
})
export class DatasListComponent implements OnInit {
  datas: Data[];
  constructor(
    private dataService: DataService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.data.subscribe( data => {
      this.datas = data['datas'];
      console.log(this.datas);
    }, error => {
      this.alertify.error(error);
    } );
  }

  deleteData(id: string) {
    this.dataService.deleteData(id).subscribe( data => {
      this.alertify.success('Data deleted successfully');
      this.datas =  this.datas.filter( p => p.id !== id);
    }, error => {
      this.alertify.error(error);
    });
  }

}
