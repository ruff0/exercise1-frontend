import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Data } from 'src/app/_models/Data';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { DataService } from 'src/app/_services/data.service';

@Component({
  selector: 'app-data-add',
  templateUrl: './data-add.component.html',
  styleUrls: ['./data-add.component.css']
})
export class DataAddComponent implements OnInit {
  data: any = { };
  @ViewChild('editForm')
  editForm: NgForm;
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }
  constructor(
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private dataService: DataService,
  ) {

  }

  ngOnInit() {

  }
  createData() {
    console.log(this.data);
    this.dataService.createData(this.data).subscribe(next => {
      this.alertify.success('Data Created Successfully');
      this.editForm.reset(this.data);
    }, error => {
      this.alertify.error(error);
    });
  }

}
