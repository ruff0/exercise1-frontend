import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Data } from '../../_models/Data';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from '../../_services/alertify.service';
import { NgForm } from '@angular/forms';
import { DataService } from '../../_services/data.service';
import { AuthService } from '../../_services/auth.service';

@Component({
  selector: 'app-data-edit',
  templateUrl: './data-edit.component.html',
  styleUrls: ['./data-edit.component.css']
})
export class DataEditComponent implements OnInit {
  data: Data;
  @ViewChild('editForm')
  editForm: NgForm;
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }
  constructor(
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private dataService: DataService,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.data = data['data'];
    });
  }
  updateData() {
    this.dataService.updateData(this.data.data.id, this.data.data).subscribe(next => {
      this.alertify.success('Data Updated Successfully');
      this.editForm.reset(this.data);
    }, error => {
      this.alertify.error(error);
    });
  }
}
