import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Data } from '../../_models/Data';
import { DataService } from 'src/app/_services/data.service';
import { AlertifyService } from 'src/app/_services/alertify.service';


@Component({
  selector: 'app-data-detail',
  templateUrl: './data-detail.component.html',
  styleUrls: ['./data-detail.component.css']
})
export class DataDetailComponent implements OnInit {
  data: Data;

  constructor(
    private dataService: DataService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.data.subscribe( data => {
      this.data = data['data'];
    });

  }

  deleteData(id: string) {
    this.dataService.deleteData(id).subscribe( data => {
      this.alertify.success('Data deleted successfully');
    }, error => {
      this.alertify.error(error);
    }, () =>  this.router.navigate(['/data']));
  }

}
