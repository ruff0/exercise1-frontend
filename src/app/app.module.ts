import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DatasListComponent } from './datas/datas-list/datas-list.component';

import { AlertifyService } from './_services/alertify.service';
import { DataService } from './_services/data.service';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';

import { ErrorInterceptorProvider } from './_services/error.interceptor';

import { DatasListResolver } from './_resolvers/datas-list.resolver';
import { DataDetailComponent } from './datas/data-detail/data-detail.component';
import { DataDetailResolver } from './_resolvers/data-detail.resolver';
import { DataEditComponent } from './datas/data-edit/data-edit.component';
import { DataEditResolver } from './_resolvers/data-edit.resolver';
import { DataAddComponent } from './datas/data-add/data-add.component';

@NgModule({
  declarations: [
    AppComponent,
    DatasListComponent,
    DataDetailComponent,
    DataEditComponent,
    DataAddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    AlertifyService,
    PreventUnsavedChanges,
    DataService,
    DatasListResolver,
    DataDetailResolver,
    DataEditResolver,
    ErrorInterceptorProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
