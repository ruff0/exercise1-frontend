import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Data } from '../_models/Data';
import { Injectable } from '@angular/core';
import { DataService } from '../_services/data.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class DataEditResolver implements Resolve<Data> {

    constructor(private datasService: DataService,
        private router: Router,
        private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Data> {
        return this.datasService.getData(route.params['id']).pipe(catchError( error => {
            this.alertify.error('Problem retrieving data');
            this.router.navigate(['/data']);
            return of(null);
        }));
    }
}
